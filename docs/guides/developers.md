# Third Room for Developers

Third Room is specifically engineered to enable developers to craft bespoke scripts for individual worlds and even extend the capabilities of the Third Room client itself. By placing the power of customization in developers' hands, Third Room fosters the creation of immersive virtual experiences that can captivate and enthrall.

## Web Scene Graph

WebSG is designed to provide a safe and performant way to manipulate glTF scene graphs from a WebAssembly module. The API can be accessed from any wasm-compatible language, giving you the flexibility to choose the programming language that best suits your needs and expertise.

There is also a custom JavaScript runtime and in-world editor, allowing you to fluidly write and edit scripts in a familiar and friendly language without leaving the Third Room client.

By utilizing the WebSG, you can create advanced and interactive experiences within Third Room that respond to user input, trigger events, and dynamically alter the virtual environment.

To get started with WebSG:

1. Familiarize yourself with the WebSG API documentation and its capabilities on the next few pages.
2. Develop your custom behaviors and interactions using WebSG to manipulate the glTF scene graph.
3. Upload your script and glTF scene by creating a new world or updating an existing world.

## Self-Hosting

Third Room can be self-hosted, giving you even more control over the user experience and content distribution. By self-hosting, you can customize the client to meet your specific needs and requirements while still benefiting from the core functionality of Third Room.

To self-host your own Third Room client:

1. Visit the Third Room GitHub repository and clone or download the source code for the client.
2. Set up your development environment with the required dependencies and tools, as outlined in the repository's README file.
3. Customize the client according to your needs, such as modifying the user interface, adding features, or integrating with other services.
4. Set up a web server to host your custom client. This can be a local server for development and testing, or a production server for public access.
5. Upload the client files to your web server and configure the server settings as necessary.
6. Test your self-hosted client to ensure it's functioning correctly and connecting to the Third Room platform.

By self-hosting your own Third Room client, you can tailor the virtual experience to suit your specific requirements and maintain complete control over the content and features available to users.

## Contributing

Third Room embraces open-source principles, and we actively encourage contributions from the developer community to help improve the platform and expand its capabilities. If you're interested in contributing to Third Room's open-source repository, follow these steps:

1. Look for open issues or feature requests in the repository's issue tracker. These issues represent opportunities for you to contribute to the project, whether by fixing bugs, implementing new features, or improving documentation.
1. Fork the repository and create a new branch for your changes.
1. Test your changes thoroughly to ensure they don't introduce new bugs or break existing functionality.
1. Submit a pull request to the main repository with your changes, including a detailed description of your contribution and any relevant issue numbers. The Third Room maintainers will review your submission and provide feedback or request changes as necessary.

By contributing to the open-source repository, you can help shape the future of Third Room and enhance the platform for users around the world. Your expertise and dedication are invaluable to the growth and success of the project, and we look forward to collaborating with you.
